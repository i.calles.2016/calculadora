
def sumar(num1, num2):
    return num1 + num2

def restar(num1, num2):
    return num1 - num2


if __name__ == "__main__":
    s1 = 1; s2 = 2
    s3 = 3;  s4 = 4

    r1 = 6; r2 = 5
    r3 = 8; r4 = 7

    suma1 = sumar(s1, s2)
    suma2 = sumar(s3, s4)

    resta1 = restar(r1, r2)
    resta2 = restar(r3, r4)

    print("Suma1: ", s1, " + ", s2, " = ", suma1)
    print("Suma2: ", s3, " + ", s4, " = ", suma2)

    print("Resta1: ", r1, " - ", r2, " = ", resta1)
    print("Resta2: ", r3, " - ", r4, " = ", resta2)



